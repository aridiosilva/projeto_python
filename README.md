# Projeto_Python

# Python - The Python Deep Learning library
Python is a dynamically typed programming language designed by Guido van Rossum. Much like the programming language Ruby, Python was designed to be easily read by programmers. Because of its large following and many libraries, Python can be implemented and used to do anything from webpages to scientific research

## Main References of Python

  - [*Python.org*](https://www.python.org/)
  - [*Get Started*](https://www.python.org/about/gettingstarted/)
  - [*Python 3.7 source code and installers*](https://www.python.org/downloads/release/python-372/)
  - [*Documentation for Python's standard library*](https://docs.python.org/)
  - [*The Python Wiki*](https://wiki.python.org/moin/)
  - [*Python Forums*](https://www.python.org/community/forums/)
  - [*Python Conferences and Workshops*](https://www.python.org/community/workshops/)
  - [*Python Developer’s Guide*](https://devguide.python.org/)
  - [*Audio/Video Talks about Python*](https://www.python.org/doc/av/)
  - [*Python Frequently Asked Questions*](https://docs.python.org/3/faq/)
  - [*Python Enhancement Proposals (PEPs)*](https://www.python.org/dev/peps/)
  - [*Python Books*](https://wiki.python.org/moin/PythonBooks)
  - [*Python Documentation*](https://www.python.org/doc/)
  - [*Python Brochure*](https://brochure.getpython.info/)
  - [*Python Source Releases*](https://www.python.org/downloads/source/)
  - [*Python Setup and Usage*](https://docs.python.org/3/using/index.html)
  - [*Python HOWTOs*](https://docs.python.org/3/howto/index.html)
  - [*Installing Python Modules*](https://docs.python.org/3/installing/index.html)
  - [*Extending and Embedding*](https://docs.python.org/3/extending/index.html)
  - [*Python/C API*](https://docs.python.org/3/c-api/index.html)
  - [*What's new in Python 3.7?*](https://docs.python.org/3/whatsnew/3.7.html)
  - [*Python Glossary*](https://docs.python.org/3/glossary.html)
 
## The Python Standard Library
Describes the standard library that is distributed with Python. It also describes some of the optional components that are commonly included in Python distributions
 
  - [*Python Standard Library 3.0*](https://docs.python.org/3/library/)
  - [*Python Global Module Index*](https://docs.python.org/3/py-modindex.html)
  - [*Python General Index - all functions, classes, terms*](https://docs.python.org/3/genindex.html)  
  - [*Matplotlib 3.0.2 documentation*](https://matplotlib.org/contents.html)
  - [*MatplotLib 3.0.2 User´s Guide*](https://matplotlib.org/users/index.html)  
   
## The Python Language Reference
Is a reference manual describing the **syntax** and **core semantics** of the language 

  - [*Python Language Reference 3.0*](https://docs.python.org/3/reference/index.html#reference-index)
  - [*Google Python Style Guide*](https://github.com/google/styleguide/blob/gh-pages/pyguide.md)
   
## The Python Tutorial

  - [*The Python Tutorial*](https://docs.python.org/3/tutorial/index.html)   
  - [*Python - Style Guide for Code (PEP 8)*](https://github.com/python/peps/blob/master/pep-0008.txt)
    
## The Python Package Index (PyPI) 
Is a repository of software for the Python programming language. PyPI helps you find and install software developed and shared by the Python community. Learn about installing packages. Package authors use PyPI to distribute their software. Learn how to package your Python code for PyPI

  - [*PyPI Package Index*](https://pypi.org/)
  - [*Python Package Common questions*](https://pypi.org/help/)
  - [*Browser Python Packages*](https://pypi.org/search/)
    
# Jupyter Notebook Projects - Interactive Computing
The Jupyter Notebook is an open-source web application that allows you to create and share documents that contain live code, equations, visualizations and narrative text. Uses include: data cleaning and transformation, numerical simulation, statistical modeling, data visualization, machine learning, and much more. The Jupyter Notebook an interactive computing framework, based on a set of open standards for interactive computing. 

 - [*Jupyter Notebook Homepage*](https://jupyter.org/)
 - [*https://jupyter.org/documentation*](https://jupyter.org/documentation)
 - [*The Juyter Notebook Stable Version*](https://jupyter-notebook.readthedocs.io/en/stable/)
 - [*Installing Jupyter Notebook*](https://jupyter.readthedocs.io/en/latest/install.html)
 - [*The Jupyter Notebook Documentation*](https://jupyter-notebook.readthedocs.io/en/stable/notebook.html)
 - [*Notebook Document Format*](https://nbformat.readthedocs.io/en/latest/format_description.html)
 - [*Jupyter Notebook Viewer*](https://nbviewer.jupyter.org/)
 - [*Notebook Widgets*](https://jupyter.org/widgets)
 - [*Jupyter widgets User Guide*](https://ipywidgets.readthedocs.io/en/stable/user_guide.html)
 - [*nbconvert - Convert Notebooks to other formats*](https://nbconvert.readthedocs.io/en/latest/)
 - [*Making kernels for Jupyter*](https://ipython.org/books.html)
 - [*Notebook Basics*](https://jupyter-notebook.readthedocs.io/en/stable/examples/Notebook/Notebook%20Basics.html)
 - [*Markdown Cells*](https://jupyter-notebook.readthedocs.io/en/stable/examples/Notebook/Working%20With%20Markdown%20Cells.html)
 - [*Jupyter Configuration Overview*](https://jupyter-notebook.readthedocs.io/en/stable/config_overview.html)
 - [*Distributing Jupyter Extensions as Python Packages*](https://jupyter-notebook.readthedocs.io/en/stable/examples/Notebook/Distributing%20Jupyter%20Extensions%20as%20Python%20Packages.html)
 - [*GitHub Reference implementation of the Jupyter Notebook format*](https://github.com/jupyter/nbformat)
 - [*Notebook Examples*](https://jupyter-notebook.readthedocs.io/en/stable/examples/Notebook/examples_index.html)
 - [*Simple Widget Introduction*](https://ipywidgets.readthedocs.io/en/stable/examples/Widget%20Basics.html)
 - [*What to do when things go wrong*](https://jupyter-notebook.readthedocs.io/en/stable/troubleshooting.html)
 *[Making a Notebook release*](https://jupyter-notebook.readthedocs.io/en/stable/development_release.html)
 
## Instaling Jupyter Notebook Kernels

 - [*Installing Python Kernel - IPython*](https://ipython.org/install.html)
 - [*Installing Kernel of Other Languages*](https://github.com/jupyter/jupyter/wiki/Jupyter-kernels)

## Jupyter Notebooks - Books

- [*(Gitub)IPython Interactive Computing and Visualization Cookbook, Second Edition*](https://github.com/ipython-books/cookbook-2nd)
- [*Mininh the Social Web, 2nd Edition*](https://nbviewer.jupyter.org/github/ptwobrussell/Mining-the-Social-Web-2nd-Edition/tree/master/ipynb/)
- [*Python for Signal Processing - Featuring Ipython Notebook *](https://nbviewer.jupyter.org/github/unpingco/Python-for-Signal-Processing/tree/master/)
- [*(GitHub)IPython in depth tutorial*](https://github.com/ipython/ipython-in-depth)
- [*Integrating Machine Learning in Jupyter Notebooks*](https://github.com/qati/GSOC16)

## Jupyter Notebooks Code Examples

- [*Notebooks for Mining the Social Web Book*](https://nbviewer.jupyter.org/github/ptwobrussell/Mining-the-Social-Web-2nd-Edition/blob/master/ipynb/Chapter%201%20-%20Mining%20Twitter.ipynb)
- [*Nbviewer for Python for Signal Processing Book*](https://nbviewer.jupyter.org/github/unpingco/Python-for-Signal-Processing/tree/master/)
- [*Notebooks for "Python for Signal Processing" book*](https://github.com/unpingco/Python-for-Signal-Processing)
- [*GitHub- Official repository for IPython itself*](https://github.com/ipython/ipython)

## Markdown - Markup Language used in Collab and Jupyter Notebooks, GitHub and other cases
A collection of awesome markdown goodies (libraries, services, editors, tools, cheatsheets, etc.)

 - [*Resources about Markdown*](https://github.com/mundimark/awesome-markdown)
 - [*GitHub-flavored markdown*](https://help.github.com/categories/writing-on-github/)
 - [*GitHub Flavored Markdown Spec*](https://github.github.com/gfm/)
 - [*Language Categories & Styles*](https://highlightjs.org/static/demo/)





